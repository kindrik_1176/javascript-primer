// This will be a showcase of my knowledge of javascript, from simple to advanced.

// Variables start with a lowercase letter:

var example = example;

// Objects and classes start with an uppercase letter:

var date = Date();

// Constants are all caps: (need to clarify)

const example = CONSTEXAMPLE;

// To keep code maintainable and readable, use a semicolon at the end of each statement:

var maintainable = readable;

// VARIABLES: Variables cannot start with a number. A variable that returns undefined:

var a;

// The above example creates a variable, or a container, that holds data. Because we have not assigned any data to it, it returns as undefined.

// Below is an example of a variable with data set in the container, or defined by using an = sign:

var a = b;

// An example use:

var c;
var d;
var result;
var c = 2;
var d = 2;
var result = c + d;

// Shorthand version of this:

var c = 2;
var d = 2;
var result = c + d;

// Data types in javascript:

// Number or Integer:
var a = 1;

// String:
var b = "example";

// Boolean:

var c = true;

// Null:

var d = null;

// Arithmitic Operators.

// The = symbol is an assignment. If you put a variable on the left, whatever is on the right, is assigned to it:

var a = b;

// Other arithmitic operators are +, -, *, and /.

var one = a + b;

var two = a - b;

var three = a * b;

var four = a / b;

// Javascript follows algebra rules. For example:

var a = 5;
var b = 5;
var c = 5;

var result = a + b * c;

// In the above example, the operators in the parentheses will happen first, so it would look like this:

var result = a + 25;

// Shorthand math can be done as follows:

a = a + 4;

// Can be rewritten as:

a += 4;

// There si shorthand for: +=, -=, *=, and /=.
